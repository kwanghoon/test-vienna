*********************************************************************
알뜰여행자를 위한 현지 정보
*********************************************************************

.. include:: ../include.def

여행을 하다 보면 계획보다 비용이 많이 드는 것을 경험하게 된다. 숙소나 식사, 쇼핑도 사전에 좋은 정보들을 많이 알고 가면 낭비를 막을 수 있는데, 이처럼 좋은 현지 정보는 여행일정과 경비를 알뜰하게 계획할 수 있도록 돕는다.


숙소
=====================================================================

모든 여행지에서 숙소는 중앙역 근처로 잡는 것이 가장 무난하지만 대부분의 도시에서 중앙역 인근은 우범지대인 경우가 많다. 따라서 중앙역 인근에서 숙소를 잡는 것을 원칙으로 하되 여 의치 않은 경우에는 교통이 편리한 곳으로 정하도록 한다. 숙소는 크게 한인민박, 현지민박, 호 스텔, 저가호텔 등으로 나눌 수 있는데 관광산업이 발달한 유럽에서는 숙박요금이 정부의 통제 를 받도록 되어 있기 때문에 허가를 받은 정식업체의 경우라면 바가지 요금을 걱정할 필요가 없 다. 따라서 가격이 비쌀수록 좋은 숙소라고 생각하면 마음 편히 예약할 수 있다. 그러나 어느 곳 을 예약하더라도 반드시 밤늦게 돌아다녀도 안전한 곳에 위치했는지는 따져보아야 한다. 한인 민박은 보통 조식으로 한식을 제공하고 교통이 편한 곳에 위치한 경우가 많기 때문에 값싸고 편 한 잠자리를 찾는 젊은 여행자들에게 인기가 많다. 그러나 예약하기 전에 반드시 유럽배낭 여행 자 카페 등에서 리뷰를 검색해서 읽어볼 것을 권한다.

비엔나는 대부분의 국제열차가 서역 Westbahnhof 에 도착하는데 이곳에서 시내 중심부로 어 차피 이동을 해야 하므로 반드시 역 주변을 고집할 필요는 없다.

.. _6-h-1:

1 까치네 민박
---------------------------------------------------------------------

비엔나 중앙역인 서역 Westbahnhof 에서 약 100미터 정도 떨어져 있다. 아침으로 한식을 제 공하며, 이외에도 추가 요금을 내면 육개장, 된장찌개, 비빔밥 등의 식사를 할 수 있다. 도미토 리 가격은 30유로부터 시작하며, 가족실도 있다.

:info-item:`홈페이지` `http://qok.me/까치네민박 <http://qok.me/까치네민박>`_

.. _6-h-2:

2 비엔나하임
---------------------------------------------------------------------

비엔나 북역에서 가깝다. 아침으로 한식을 제공하며, 남녀가 같이 사용하는 도미토리는 30유로 부터 시작. 1인실, 2인실, 가족실도 있다.

:info-item:`홈페이지` `http://qok.me/비엔나하임 <http://qok.me/비엔나하임>`_

.. _6-h-3:

3 소미네집
---------------------------------------------------------------------

서역과 비엔나 중심부 중간 정도에 위치해 있다. 아침으로 한식을 제공하며, 도미토리는 30유 로부터 시작한다. 가족실은 사용인원에 따라 요금을 받는다.

:info-item:`홈페이지` `http://qok.me/소미네집 <http://qok.me/소미네집>`_

.. _6-h-4:

4 믿거나말거나 Believe-it-or-Not
---------------------------------------------------------------------

비엔나에서 최고 인기 호스텔. 현대식 아파트 2채에서 도미토리를 운영하며, 가격은 30유로 정 도이다. 호스텔월드에서 예약할 수 있다.

:info-item:`주소` `Myrthengasse 10, 1070 Wien  <https://maps.google.com/maps?q=Myrthengasse+10+/+14,+A-1070+WIEN&hl=ko&ie=UTF8&sll=48.198881,16.353428&sspn=0.001436,0.001751&hnear=Myrthengasse+10,+Neubau+1070+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=r0>`_

:info-item:`홈페이지` `http://qok.me/믿거나말거나 <http://qok.me/믿거나말거나>`_

.. _6-h-5:

5 움밧시티 Wombats City The Lounge
---------------------------------------------------------------------

서역에서 도보로 5분 이내의 거리에 있는 비엔나 인기 숙소이다. 8명의 남녀가 한 방을 쓰는 도 미토리는 가격이 18유로부터 시작한다. 2인실도 있지만, 인기가 많기 때문에 사전예약은 필수. 움밧시티 홈페이지나 호스텔월드 등에서 예약할 수 있다.

:info-item:`주소` `Rechte Wienzeile 35, 1040 Wien <https://maps.google.com/maps?q=Rechte+Wienzeile+35,+1040+Wien&hl=ko&ie=UTF8&sll=48.205318,16.347707&sspn=0.013829,0.035727&hnear=Rechte+Wienzeile+35,+Wieden+1040+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=17>`_

:info-item:`홈페이지` `http://qok.me/움밧시티 <http://qok.me/움밧시티>`_

.. _6-h-6:

6 래디슨 블루 비엔나
---------------------------------------------------------------------

래디슨 호텔 체인으로 슈타트파크 인근에 위치해 있다. 가격은 1박 2인실 기준으로 127유로 부터이며, 대부분의 호텔이 그렇듯이 예약날짜에 따라 가격은 천차만별이니 주의할 것. 호텔 홈페 이지를 직접 이용하거나 트립어드바이저나 북킹닷컴 같은 예약사이트에서 예약할 수 있다.

:info-item:`주소` `Parkring 16, 1010 Wien <https://maps.google.com/maps?q=Parkring+16,+1010+Wien&hl=ko&ie=UTF8&hnear=Parkring+16,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16>`_

:info-item:`홈페이지` `http://qok.me/래디슨블루비엔나 <http://qok.me/래디슨블루비엔나>`_

.. _6-h-7:

7 르메르디앙 비엔나
---------------------------------------------------------------------

오페라하우스 인근에 있는 현대식 호텔이다. 가격은 1박에 2인실 기준 185유로 정도이다. 예약 은 호텔 홈페이지나 북킹닷컴에 들어가면 할 수 있으며, 시간적인 여유가 있다면 프라이스라인 에 들어가서 비딩을 해보는 것도 괜찮다.

:info-item:`주소` `Opernring 13, 1010 Vienna <https://maps.google.com/maps?q=Opernring+13,+1010+Vienna,+Austria&hl=en&hnear=Opernring+13,+Innere+Stadt+1010+Wien,+Austria&t=m&z=16>`_

:info-item:`홈페이지` `http://qok.me/르메르디앙비엔나 <http://qok.me/르메르디앙비엔나>`_



:link-title:`숙소검색은 여기서!`

`북킹닷컴 <http://www.booking.com/>`_ : 전 세계 호텔을 실시간으로 검색해서 예약할 수 있는 사이트로 한국어 서비스가 제공된다. 숙박료의 통화 단위 도 편한 대로 바꿔가며 요금을 조회할 수 있다.

`트립어드바이저 <http://www.tripadvisor.co.kr/>`_ : 북킹닷컴과 마찬가지로 전 세계 호텔을 실시간으로 검색하는 것이 가능하다. 최고급 호텔부터 저렴한 여관급 까 지 모두 다루고 있기 때문에 선택의 폭이 넓다.

`호스텔월드 <http://www.hostelworld.com/>`_ : 전 세계 호스텔과 저가 호텔을 검색할 수 있는 사이트로 한국어 서비스가 제공된다. 실시간 숙박현황을 조회할 수 있으며, 요금도 유로나 달러, 원화 등으로 지정해서 알아볼 수 있다.

`이비스호텔 <http://ibishotel.ibis.com/gb/home/index.shtml>`_ : 중저가 체인호텔로 유럽 전역에 있다. 호스텔과 달리 도미토리는 없기 때문에 가족이나 두세명이 한 방을 쓰려는 여행객들에게 유용하다.

`네이버 유랑 카페 <http://cafe.naver.com/firenze>`_ : 국내 대표적인 여행카페로 유럽 배낭여행자라면 한번쯤은 들어봤을 만큼 활동이 회원들의 활발하다. 계속해서 현지 정보나 맛집 리뷰, 숙소 리뷰 등이 올라오기 때문에 참고할만한 정보가 많다.

`프라이스라인 <http://qok.me/프라이스라인>`_ : 항공권, 호텔, 렌트카, 패키지 여행상품 등을 판매하는 여행 관련 온라인 사이트이다. 가장 큰 장점은 “Name your price” 란 슬로건 답게 경매 방식으로도 상품을 구입할 수 있다는 점이다. 운이 좋으면 항공권은 40%, 호 텔은 60%, 렌트카는 40% 까지도 할인이 가능하다.



맛집
=====================================================================

맛집에 대한 평가는 여행자에 따라 다를 수 있다. 본 책자는 저자의 경험과 여러 여행자들의 평 가를 바탕으로 맛집을 소개하고 있는데, 절대적인 기준이 아니니 여행시 참고만 하기를 바란다.

.. _6-c-1:

1 피그뮬러
---------------------------------------------------------------------

.. figure:: /_static/figures/6-c-1.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`맛집 1` 피그뮬러 현판 및 슈니첼

피그뮬러 Zum Figmuller 는 오스트리아의 대표적인 음식인 슈니첼을 부담없이 맛 볼수 있는 비엔나의 대표적인 맛집이다. 슈테판 성당 가까이 있기도 하거니와 맛과 가격이 모두 만족스러 워 항상 붐비는 곳이므로 가능하면 식사시간을 피해서 가는 것이 좋다. 새콤한 감자샐러드와 같 이 곁들여 먹다보면 어느새 그 큰 슈니첼을 모두 먹어치운 자신을 발견하게 된다.

:info-item:`주소` `Wollzeile 5, 1010 Wien <https://maps.google.com/maps?q=Figlm%C3%BCller+Wollzeile,+Wollzeile,+Wien,+%C3%96sterreich&hl=ko&ie=UTF8&oq=fig,+Wien,+%C3%96sterreich&hq=Figlm%C3%BCller+Wollzeile,+Wollzeile,&radius=15000&t=m&z=18&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/피그뮬러 <http://qok.me/피그뮬러>`_

.. admonition:: |tip-icon| :note-title:`알름두들러`

   .. figure:: /_static/figures/6-c-1-1.jpg
         :align: center
         :figclass: figure-img

         오스트리아의 인기 음료수인 알름두들러 Almdudler 는 포도와 사과 쥬스에 살짝 허브 맛이 감도는 탄산음 료이다. 진저 에일과 맛이 조금 비슷하다고는 하지만 알름두들러 만의 독특한 향과 맛을 갖고 있다. 오스트 리아에서 코카콜라 다음으로 가장 잘 팔리는 음료수이기 때문에 ‘오스트리아의 국민음료’라고도 불린다. 오 스트리아 인근의 체코, 스위스, 독일, 헝가리, 벨기에 등에서도 만날 수 있는데, 일단 오스트리아에 갔다면 국민음료인 알름두들러를 한번 맛보는 것도 좋은 생각이다.

         :info-item:`홈페이지` `http://qok.me/알름두들러 <http://qok.me/알름두들러>`_


.. _6-c-2:

2 립스 오브 비엔나
---------------------------------------------------------------------

.. figure:: /_static/figures/6-c-2.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`맛집 2` 립스 오브 비엔나 입구

립스 오브 비엔나 Ribs of Vienna 는 이제 우리나라 여행자들에게도 많이 알려져 있는 바베큐 립 전문점이다. 시내에 있기 때문에 돌아다니다가 편안한 분위기에서 시원한 맥주 한잔과 든든 한 식사가 하고 싶을 때, 들리면 좋다. 한국의 패밀리레스토랑에서 맛보는 미국식 바베큐립 요 리와는 소스가 많이 다르다.

:info-item:`주소` `Weihburggasse 22, 1010 Wien <https://maps.google.com/maps?q=Ribs+of+Vienna,+Weihburggasse,+Wien,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.209646,16.373663&sspn=0.011483,0.014012&oq=ribs+ofWien&hq=Ribs+of+Vienna,+Weihburggasse,+Wien,+%C3%96sterreich&t=m&z=17&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/립스오브비엔나 <http://qok.me/립스오브비엔나>`_

.. admonition:: |tip-icon| :note-title:`고서`

   .. figure:: /_static/figures/6-c-1-1.jpg
         :align: center
         :figclass: figure-img

         부드러운 맥주를 좋아하는 사람이라면 오스트리아의 맥주인 시원한 고서 Gösser 한 잔을 추천한다. 부드 러운 맛과 향이 특징인 고서는 한번 마셔본 사람들이라면 그 맛을 잊지 못하고 반드시 다시 찾는다고 한다.

         :info-item:`홈페이지` `http://qok.me/고서 <http://qok.me/고서>`_


.. _6-c-3:

3 노드씨
---------------------------------------------------------------------

.. figure:: /_static/figures/6-c-3.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`맛집 3` 노드씨 내부 및 입구

노드씨 Nordsee 는 유럽에 많은 체인점을 갖고 있는 독일계 패스트푸드 레스토랑이다. 본래는 독일 브레멘 지역에 북해산 해산물을 공급하던 업체였지만, 지금은 해산물을 주 재료로 한 간단 한 음식들을 파는 레스토랑으로 영업을 하고 있다. 이곳은 해산물을 재료로 한 샐러드, 샌드위 치 등 패스트푸드를 서빙 하므로 여행하면서 신선한 야채와 생선요리가 먹고 싶을 때 한번 들려 볼 만 한다. 비엔나 시내에도 매장이 여러 군데 있지만, 여기서 소개하는 매장은 슈테판 성당에서 가깝다.

:info-item:`주소` `Rotenturmstraße 4, 1010 Wien <https://maps.google.com/maps?q=Rotenturmstra%C3%9Fe+4,+1010+Wien&hl=ko&ie=UTF8&sll=48.203912,16.369586&sspn=0.011484,0.014012&hnear=Rotenturmstra%C3%9Fe+4,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/노드씨 <http://qok.me/노드씨>`_

.. _6-c-4:

4 카페 자허
---------------------------------------------------------------------

.. figure:: /_static/figures/6-c-4.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`맛집 4` 카페 자허 내부 및 자허 토르테

보통 비엔나 하면 흔히들 떠올리는 것이 비엔나 커피이지만 이 커피와 가장 잘 어울리는 케이크 또한 비엔나에 있다. 카페 자허 CaféSacher 는 본래 자허 호텔에서 운영하던 작은 커피집이였 는데 이곳에서 자허 토르테 Sacher Torte를 처음 만들면서 비엔나를 대표하는 케이크집이 되 었다. 이제는 비엔나 어느 곳에서나 자허 토르테를 맛볼 수 있지만, 아직까지도 원조를 맛보기 위해 수많은 관광객들이 찾고 있다. 참고로 비엔나에는 사실 비엔나 커피가 없고, 아인슈패너가 우리가 생각하는 비엔나 커피와 가장 흡사한 커피이다.

:info-item:`주소` `Philharmonikerstraße 4, 1010 Wien <https://maps.google.com/maps?q=Philharmonikerstra%C3%9Fe+4,+1010+Wien&hl=ko&ie=UTF8&sll=48.208576,16.367249&sspn=0.011483,0.014012&hnear=Philharmonikerstra%C3%9Fe+4,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/카페자허 <http://qok.me/카페자허>`_

.. _6-c-5:

5 데멜
---------------------------------------------------------------------

.. figure:: /_static/figures/6-c-5.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`맛집 5` 데멜 외부

오스트리아 씨씨 황후에게 케이크를 바치던 베이커리라는 명성을 갖고 있는 데멜 Demel 은 비엔나 최고의 케이크를 만드는 곳으로 유명하다. 데멜은 주방벽을 유리로 하여 케이크를 만드 는 과정을 손님들에게 공개하고 있는데 만드는 케이크 리스트만 수백 가지가 넘는다고 한다. 케 이크는 매장에서 주문하고, 커피는 자리에 앉은 후 주문하면 된다.

:info-item:`주소` `Kohlmarkt 14, 1010 Wien <https://maps.google.com/maps?q=Demel+Konditorei,+Kohlmarkt,+Wien,+Austria&hl=ko&ie=UTF8&sll=48.208599,16.3672&sspn=0.045931,0.056047&oq=demel+Wien,+Austria&hq=Demel+Konditorei,&hnear=Kohlmarkt,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/데멜 <http://qok.me/데멜>`_

.. _6-c-6:

6 슈바이처하우스
---------------------------------------------------------------------

.. figure:: /_static/figures/6-c-5.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`맛집 5` 슈바이처하우스 입구

슈바이처하우스 Schweiserhaus 는 유명 여행가이드인 론리 플래닛에서 꼽은 비엔나 맛집 중 하나이다. 프라터 인근에 위치한 야외 맥주집으로 시원한 맥주와 일종의 돼지족발 요리인 Hintere Schweinsstelze을 알아준다.

:info-item:`주소` `Prater 116, 1020 Wien <https://maps.google.com/maps?q=schweizerhaus,+Wien,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.203912,16.369586&sspn=0.005742,0.007006&hq=schweizerhaus,+Wien,+%C3%96sterreich&t=m&z=15>`_

:info-item:`홈페이지` `http://qok.me/슈바이처하우스 <http://qok.me/슈바이처하우스>`_

.. _6-c-7:

7 킴 코흐트
---------------------------------------------------------------------

.. figure:: /_static/figures/6-c-5.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`맛집 5` 킴 코흐트 입구

한국인 쉐프 김소희씨가 운영하는 퓨전 한국식당 킴 코흐트 Kim Kocht 는 비엔나 현지인들이 더 많이 찾는 고급 레스토랑이다. 저녁식사 예약을 하려면 몇 달을 기다려야 할 정도로 인기가 많은데 Kocht 는 독일어로 ‘요리하다’란 의미이다. 점심식사는 코스요리가 아닌 경우, 예약이 필요 없다. 홈페이지가 한글로도 제공되니 방문할 계획이라면 한번 살펴보는 것이 좋다.

:info-item:`주소` `Lustkandlgasse 4, 1090 Wien <https://maps.google.com/maps?q=Lustkandlgasse+4,+A-1090+Wien&hl=ko&ie=UTF8&sll=48.199206,16.364838&sspn=0.002871,0.003503&hnear=Lustkandlgasse+4,+Alsergrund+1090+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=17&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/킴코흐트 <http://qok.me/킴코흐트>`_


쇼핑
=====================================================================

본 책자에서 쇼핑 정보는 현지의 특산품 위주로 소개하고 있다. 프라하는 구시가 광장을 비롯한 시내 곳곳에 특산품을 파는 상점이 산재해 있기 때문에 길을 걷다가 마음에 드는 상점에 들어가 쇼핑을 하는 것이 좋을 것이다. 여기서는 프라하에서 흔히 볼 수 있는 기념품과 공예품을 몇 가 지 소개하고자 한다.

.. _6-s-1:

1 스와로브스키
---------------------------------------------------------------------

.. figure:: /_static/figures/6-s-1.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`쇼핑 1` 스와로브스키 건물

스와로브스키 Swarovski 는 1895년 오스트리아의 다니엘 스와로브스키가 세운 회사이다. 크 리스탈을 가공하여 작은 소품부터 그릇, 장신구 등에 이르기 까지 10만 개 이상의 제품을 생산 하고 있다. 지금은 우리나라를 비롯하여 전 서계에 매장이 있지만 오스트리아를 방문했다면 한 번 들려볼 만 하다. 비엔나 시내에 있는 몇 군데의 매장 중 여기에 소개된 매장은 케른트너 거리 에 위치한다.

:info-item:`주소` `Kärntner Straße 24, 1010 Wien <https://maps.google.com/maps?q=Swarovski+Wien,+K%C3%A4rntner+Stra%C3%9Fe,+Vienna,+Austria&hl=ko&ie=UTF8&ll=48.205821,16.371002&spn=0.004841,0.011008&sll=48.208459,16.367655&sspn=0.011483,0.014012&oq=swarov&hq=Swarovski&hnear=K%C3%A4rntner+Stra%C3%9Fe,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=17&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/스와로브스키 <http://qok.me/스와로브스키>`_

.. _6-s-2:

2 아우가르텐 스토어
---------------------------------------------------------------------

.. figure:: /_static/figures/6-s-2.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`쇼핑 2` 아우가르텐 스토어 내부

아우가르텐 Augarten 도자기는 합스부르크 왕가에서 사용하면서 전 세계에 그 이름을 알리게 되었다. 우리에게 가장 잘 알려진 디자인은 ‘비엔나의 장미’로 핑크빛 장미가 깔끔하게 새겨져 있는 시리즈이다. 비엔나 시내 북쪽의 아우가르텐 공원에 아직도 아우가르텐 공방이 있어 수작 업으로 도자기를 생산하고 있으니, 아우가르텐 공원을 둘러볼 계획이라면 한번 방문해 볼만 하 다. 참고로 아우가르텐 공원에 있는 아우가르텐 궁전은 비엔나 소년합창단의 본부로 사용되고 있다. 여기에서 소개하고 있는 아우가르텐 스토어는 슈테판 성당 인근에 위치해 있다.

:info-item:`주소` `Spiegelgasse 3, 1010 Wien <https://maps.google.com/maps?q=Spiegelgasse+3,+1010+Wien&hl=ko&ie=UTF8&sll=48.20537,16.372526&sspn=0.005742,0.007006&hnear=Spiegelgasse+3,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/아우가르텐스토어 <http://qok.me/아우가르텐스토어>`_

.. _6-s-3:

3 만츠
---------------------------------------------------------------------

.. figure:: /_static/figures/6-s-3.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`쇼핑 3` 만츠 입구

만츠 Manz 서점은 비엔나 시내에 세 군데 정도 있는데 콜마크트 거리에 있는 만츠는 1912년 오픈해서 지금까지 100년 동안 계속해서 문을 열고 있다. 개업 당시 건축가 아돌프 로스가 건 물 정면부와 실내 인테리어를 맡아 유명하다. 주로 독일어 서적이 많기는 하지만 엽서나 간단한 책 관련 소품도 판매하고 있다.

:info-item:`주소` `Kohlmarkt 16, 1010 Wien <https://maps.google.com/maps?q=Kohlmarkt+16,+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&hl=ko&ie=UTF8&sll=48.208459,16.371775&sspn=0.023652,0.026736&hnear=Kohlmarkt+16,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/만츠 <http://qok.me/만츠>`_

.. _6-s-4:

4 마너
---------------------------------------------------------------------

.. figure:: /_static/figures/6-s-4.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`쇼핑 4` 마너 내부

오스트리아가 세계적으로 자랑하는 간식거리 마너 Manner 에서 가장 인기있는 제품은 헤이즐 넛 크림 웨하스인 Neapolitan wafers 이다. 이 제품은 1898년 처음 등장했는데 당시 이태리 나폴리에서 수입한 헤이즐넛으로 만든 크림을 웨하스에 넣었다. 이 제조법은 오늘날까지도 지 켜지고 있다.

:info-item:`주소` `Stephansplatz 7, 1010 Wien <https://maps.google.com/maps?q=Stephansplatz+7,+1010+Wien,+%C3%96sterreich,+01+5137018&hl=ko&ie=UTF8&sll=48.208888,16.373641&sspn=0.001435,0.001751&hnear=Stephansplatz+7,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/마너 <http://qok.me/마너>`_

.. admonition:: |tip-icon| :note-title:`마너의 로고`

   .. figure:: /_static/figures/6-s-4-1.jpg
         :align: center
         :figclass: figure-img

         마너 제품을 잘 들여다 보면 슈테판 성당이 로고로 사용되는 것을 볼 수 있다. 그 이유는 마너의 창업자인 요셉 마너가 슈테판 성당 근처에 첫 가게를 열 때 슈테판 성당의 이미지를 회사 로고로 사용하는 대신 슈테 판 성당의 보수일을 하는 석공 한 명의 임금을 대신하여 지불하기로 비엔나 교회측과 계약을 했기 때문이 다.

.. _6-s-5:

5 나슈마크트
---------------------------------------------------------------------

.. figure:: /_static/figures/6-s-5.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`쇼핑 3` 나슈마크트 거리

나슈마크트 Naschmarkt 는 월요일부터 토요일까지 오픈하는 비엔나에서 가장 큰 재래시장이 다. 생화와 야채, 치즈 및 유제품, 그리고 과일 등이 넘쳐나는 활기찬 곳으로 한국식료품점도 중 간 정도에 위치해 있다. 또한, 매주 토요일에는 나슈마크트의 공터에서 각종 중고물품을 거래하 는 벼룩시장도 열리니 참고할 것.

:info-item:`주소` `Naschmarkt, Wien <https://maps.google.com/maps?q=Naschmarkt,+Wien,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.207889,16.370525&sspn=0.011483,0.014012&oq=na&hq=Naschmarkt,+Wien,+%C3%96sterreich&t=m&z=18>`_

:info-item:`홈페이지` `http://qok.me/나슈마크트 <http://qok.me/나슈마크트>`_
