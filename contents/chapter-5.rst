*********************************************************************
비엔나 링 외부 지역
*********************************************************************

.. include:: ../include.def

비엔나 링 외부지역은 시내 중심부에서 다소 떨어져있기 때문에 일정이 바쁜 여행자들은 대부 분 링 내부나 주변 지역만 살펴보고 바쁘게 비엔나를 떠나는 경우가 많다. 그러나 비엔나를 제대로 이해하려면 비엔나 링 외부지역을 포함해서 외곽 지역들도 한번쯤 들려보는 것이 좋다. 여 기서는 링 외부지역에서 꼭 들려볼만한 곳들을 정리해놓았다.

.. _5-18:

18 프로이드 박물관
---------------------------------------------------------------------

.. figure:: /_static/figures/5-18.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 18` 프로이드 박물관 입구

1891년부터 1938년까지 프로이드가 거주했던 곳을 프로이드 박물관 Freud Museum 으로 사용하고 있다. 프로이드는 2차 세계대전이 발발하여 나치에게 쫓겨 런던으로 망명하기 전까지 이곳에서 환자들을 진료하며 <꿈의 해석>을 저술했다. 프로이드의 막내딸 안나 프로이드가 주도가 되어 박물관을 설립했는데 환자들의 진료기록과 그의 유품들을 볼 수 있다. 지하철 2호선 을 타고 Schottenring 역에서 내려서 4분 정도 걸어야 한다.

:info-item:`주소` `Berggasse 19, 1090 Wien <https://maps.google.com/maps?q=freud+museum,+wien&hl=ko&ie=UTF8&sll=48.213175,16.36007&sspn=0.045927,0.06197&hq=freud+museum,&hnear=%EB%B9%84%EC%97%94%EB%82%98+%EB%B9%88+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=14&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/프로이드박물관 <http://qok.me/프로이드박물관>`_

.. admonition:: |tip-icon| :note-title:`프로이드`

   .. figure:: /_static/figures/5-18-1.jpg
         :align: center
         :figclass: figure-img

         지그문트 프로이드 Sigmund Freud(1856~1939) 는 비엔나 의과대학에서 생리학을 전공하고, 현재 프로 이드 박물관으로 쓰이는 Berggasse 19번지 건물에서 신경질환 전문의로 개업을 했다. 환자를 진료하면서 정신질환자가 아닌 일반인을 심리분석을 통해 치료하는 정신분석학 이론을 정립했고 저서 <꿈의 해석>과 그가 설립한 비엔나 정신분석학회의 활발한 활동으로 세계적인 명성을 얻었다. 그러나 이론적인 입장 차이 때문에 심리학자 아들러나 융과 불화가 생기기도 했지만, 1933년 유대인이라는 이유로 나치의 탄압을 피 해 영국으로 망명할 때까지 비엔나를 거점으로 해서 활발한 연구활동을 펼쳤다.


.. _5-19:

19 세체지온
---------------------------------------------------------------------

.. figure:: /_static/figures/5-19.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 19` 세체지온 정면 및 베토벤 프리즈

1897년 구스타브 클림트를 주축으로 한 예술가들이 예술의 자유를 주장하며 비엔나 예술협회 를 탈퇴하여 분리라는 뜻의 세체지온 Secession 이란 새로운 예술협회를 설립했다. 자신들을 위한 전시공간으로 세체지온은 비엔나 분리파 예술가들 뿐만 아니라 외국 작품들을 소개하면 서 오스트리아에 새로운 예술의 기운을 불어 넣는데 큰 공헌을 했다. 클림트의 <베토벤 프리즈 >란 작품이 소장되어 있다. 분리파에 소속되었던 예술가로는 에곤 쉴레 Egon Schiele, 오스카 코코슈카 Oskar Kokoschka, 오토 바그너 Otto Wagner 등이 있다. 지하철 1번 칼스플라츠 Karlsplatz 역에서 내려 나슈마크트 방향으로 걸어서 5분이다.

:info-item:`개장시간` 화~ 일 10:00~18:00, 월요일 휴관

:info-item:`입장료` 8.5유로

:info-item:`주소` `Innere Stadt, 1010 Vienna <https://maps.google.com/maps?q=secession,+wien&hl=ko&ie=UTF8&sll=48.218668,16.36302&sspn=0.045922,0.06197&hq=secession,&hnear=%EB%B9%84%EC%97%94%EB%82%98+%EB%B9%88+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/세체지온 <http://qok.me/세체지온>`_

.. _5-20:

20 뮤지엄 카르티에
---------------------------------------------------------------------

.. figure:: /_static/figures/5-20.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 20` 뮤지엄 카르티에 정면

뮤지엄 카르티에 Museum quartier 는 넓은 공간에 레오폴트 미술관, 쿤스트할레, 카르티에 21 등의 미술관과 서점, 카페, 공연장 등을 수용한 문화복합단지이다. 본래 합스부르크 왕가의 마굿간으로 사용하던 건물로 여러 차례의 확장을 통해 공간을 넓혔지만, 자동차의 등장으로 쓸 모가 없게 된 거대한 마굿간을 문화공간으로 탈바꿈시켰다.

:info-item:`주소` `Museumsplatz 1, 1070 Wien <https://maps.google.com/maps?q=museum+quartier,+wien&hl=ko&ll=48.204255,16.364179&spn=0.009682,0.022016&sll=48.204227,16.369266&sspn=0.011484,0.015492&hq=museum+quartier,+wien&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/뮤지엄카르티에 <http://qok.me/뮤지엄카르티에>`_


.. _5-21:

21 칼스교회
---------------------------------------------------------------------

.. figure:: /_static/figures/5-21.jpg
     :align: center
     :figclass: figure-img

     :figure-number:`그림 21` 칼스 교회 정면

유럽 전역을 휩쓴 페스트에서 비엔나가 벗어난 것을 기념하여 1713년 카를 6세가 건축가 엘라 흐 Erlach에게 지시하여 칼스교회 Karlskirche 를 건축했다. 높이 72미터의 푸른색 돔은 바티칸의 성베드로 성당에서, 좌우 33미터의 높이의 원기둥은 로마의 트라야누스 기념비에서 각각 힌트를 얻었다고 전해지는 바로크 양식의 건축물이다. 화려한 내부가 볼만한 데 천장을 떠받치 는 대리석 기둥과 벽화 등이 관광객의 시선을 끈다. 지하철 1번 Karlsplatz 역에서 내려 걸어가 면 된다. 교회 옆에는 브람스가 마지막 숨을 거둔 집이 있었는데 현재는 존재하지 않고, 단지 교 회 인근의 브람스 동상만이 브람스의 존재를 알려주고 있다.

:info-item:`주소` `Kreuzherrengasse 1, 1040 Wien <https://maps.google.com/maps?q=Karlskirche&hl=ko&sll=48.203869,16.364136&sspn=0.011484,0.015492&hq=Karlskirche&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/칼스교회 <http://qok.me/칼스교회>`_

.. admonition:: |tip-icon| :note-title:`브람스`

   .. figure:: /_static/figures/5-21-1.jpg
         :align: center
         :figclass: figure-img

         요하네스 브람스 Johannes Brahms(1833~1897) 는 독일 함부르크에서 태어났다. 1862년부터 비엔나 에 거주하며 창작과 연주생활을 한 이 작곡가는 슈만과 친분을 맺으면서 슈만의 부인인 클라라를 평생 연모 했다. 칼스교회 인근의 자택에서 간암으로 사망한 브람스는 비엔나 중앙공원에 묻혔다.


.. _5-22:

22 벨베데레 궁전
---------------------------------------------------------------------

.. figure:: /_static/figures/5-22.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 22` 벨베데레 궁전 전경

벨베데레 궁전 Schloss Belvedere 은 터키와의 전쟁에서 비엔나를 구한 전쟁영웅 오이겐공 의 궁전이었지만, 오이겐공이 사망한 후에는 합스부르크 왕가의 미술품을 수집해 두는 용도로 사용되었다. 사라예보에서 암살당한 페르디난드 황태자가 1914년까지 이곳에서 거주했다. 1955년 5월 미,소,영,불 4개국의 외무장관들이 10년간의 신탁통치를 마치고 오스트리아의 완 전한 자유와 독립을 선언한 곳이기도 하다.

1716년 별궁인 하궁이 먼저 만들어졌고, 1723년 연회장 용도의 상궁을 지었다. 현재 상궁은 19,20세기 회화관, 하궁은 오스트리아 미술관으로 사용되고 있고, 상궁과 하궁 사이에는 프랑스식 정원이 있다. 상궁에는 우리나라에도 많이 알려진 오스트리아 화가 구스타프 클림트, 에곤 쉴레, 오스카 코코슈카의 작품들이 걸려 있으며, 클림트의 명작 <키스>도 이곳에 전시되어 있 다.

오페라 하우스에서 도보로 10분 정도 걸린다. 전차는 D번의 경우 Schloss Belvedere 에서 하차, 전차 71번의 경우에는 Unteres Belvedere 에서 하차한다.

:info-item:`개장시간` 매일 10:00~18:00, 수요일만 하궁은 21:00까지 오픈

:info-item:`입장료` 10유로(상하궁 모두 입장 시)

:info-item:`주소` `Prinz-Eugen-Straße 27, 1040 Wien <https://maps.google.com/maps?q=Schloss+Belvedere&hl=ko&ll=48.196045,16.373577&spn=0.009683,0.022016&sll=48.201023,16.368062&sspn=0.011484,0.015492&hq=Schloss+Belvedere&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/벨베데레궁전 <http://qok.me/벨베데레궁전>`_

.. _5-23:

23 쉔브룬 궁전
---------------------------------------------------------------------

.. figure:: /_static/figures/5-23.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 23` 쉔브룬 궁전 전경

쉔브룬 궁전 Schloss Schöenbrunn 은 합스부르크 왕가의 여름별궁으로 1996년 유네스코 에 의해 세계의 문화유산으로 지정되었다. 1612년 마티아스 황제가 사냥 도중 발견한 아름다운 Schoen 샘 Brunn 에서 그 이름이 유래되었다고 하는데, 프랑스와의 세력 다툼이 한창 일 때 프랑스 베르사이유 궁전과 경쟁하려고 증축을 시작했다. 그러나 전쟁으로 자금이 고갈되자 규 모를 줄여 지금의 크기로 완공되었다. 쉔브룬 궁전의 실내는 대부분 로코코 양식으로 꾸며져 있 는데 마리아 테레지아 여제, 프란츠 요셉 황제, 씨씨 황후 등이 이곳에서 생활했다. 합스부르크 왕가의 유물이 전시되어 있는 궁전 내부는 현재 1441개의 방중에서 45개의 방만 일반에게 공 개 되고 있다. 1918년 1차 세계 대전 패전했을 때 당시 황제 카를 1세가 오스트리아 헝가리제 국의 종말을 선언한 곳이기도 하다. 궁전 내부투어에는 몇 가지 종류가 있는데 일반 여행자의 경우라면 임페리얼 투어 정도면 충분 하다. 지하철 4번을 타고 Schoenbrunn 역에서 내리면 된다.

:info-item:`개장시간` 매일 08:30~18:30(폐관시간은 계절에 따라 달라지니 미리 홈페이지에서 확인)

:info-item:`입장료` 10.5~15.5유로

:info-item:`주소` `Schönbrunn Palace Schönbrunner Schloßstraße 47 1130 Wien <https://maps.google.com/maps?q=%EC%87%A4%EB%B8%8C%EB%A3%AC+%EA%B6%81%EC%A0%84+Sch%C3%B6nbrunn+Palace+Sch%C3%B6nbrunner+Schlo%C3%9Fstra%C3%9Fe+47+1130+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&hl=ko&ie=UTF8&sll=48.196474,16.375315&sspn=0.011485,0.015492&geocode=FSxE3wIdzOj4AA&hnear=Sch%C3%B6nbrunn+Palace,+Sch%C3%B6nbrunner+Schlo%C3%9Fstra%C3%9Fe+47,+1130+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&hq=%EC%87%A4%EB%B8%8C%EB%A3%AC+%EA%B6%81%EC%A0%84&z=14&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/쉔브룬궁전 <http://qok.me/쉔브룬궁전>`_

.. _5-24:

24 프라터 박물관
---------------------------------------------------------------------

.. figure:: /_static/figures/5-24.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`그림 24` 프라터 박물관 입구

합스부르크 왕가의 사냥터였던 프라터 박물관 Pratermuseum 은 1766년 시민에게 개방되었 는데 제2차 세계대전이 막바지에 이른 1945년에 완전히 파괴되었다. 영화 <제3의 사나이>, <비포선라이즈> 촬영장소로 더욱 유명한 이곳은 사실 박물관이라기 보다는 놀이공원에 가깝다. 휴일에는 비엔나 사람들로도 상당히 붐비며 회전목마, 대전차, 청룡열차 등의 놀이기구들이 있 다. 입장료는 따로 내지 않고 놀이기구를 탈 때마다 이용료를 지불 하는 방식이다. 1번 지하철 Praterstern/Wien Nord 역에서 내려 걸어가거나 시내에서 전차 21번, N번을 이 용하여 갈 수 있다.

:info-item:`주소` `Oswald-Thomas-Platz 1, 1020 Wien <https://maps.google.com/maps?q=Oswald-Thomas-Platz+1,+1020+Wien&hl=ko&ie=UTF8&hnear=Oswald-Thomas-Platz+1,+Leopoldstadt+1020+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16>`_

:info-item:`홈페이지` `http://qok.me/프라터박물관 <http://qok.me/프라터박물관>`_

.. _5-25:

25 쿤스트하우스
---------------------------------------------------------------------

.. figure:: /_static/figures/5-25.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`그림 25` 쿤스트 하우스 입구

자연계에 직선은 존재하지 않는다고 믿는 훈데르트밧서가 설계한 미술관이다. 자연스러운 곡선 과 식물로 덮인 이 건물은 훈데르트밧서의 철학을 잘 보여주는 대표작으로 손꼽힌다. 몬드리안의 그림이 건축물로 재현된 듯 한 느낌의 쿤스트하우스 Kunsthaus 는 현재 훈데르트밧서의 작 품을 전시하는 미술관으로 사용되고 있다. 이곳에서 5분 정도 걸어가면 그의 또 다른 대표작 훈 데르트밧서 하우스 Hundertwasser Haus 가 있다. 비엔나 시에서 훈데르트밧서에게 의뢰하 여 설계한 시영주택으로 현재까지도 주민들이 거주하고 있으니 들려보면 좋다. 지하철 1번, 4번을 타고 Schwedenplatz 역에서 내려 1번 전차로 갈아탄 후 Radetzkyplatz 역에서 하차한다.

:info-item:`개장시간` 매일 10:00~19:00

:info-item:`입장료` 10유로

:info-item:`주소` `Untere Weißgerberstraße 13, 1030 Wien <https://maps.google.com/maps?q=KunstHaus,+Wei%C3%9Fgerberl%C3%A4nde,+Landstra%C3%9Fe,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.216236,16.394952&sspn=0.011481,0.015492&oq=kunsth&hq=KunstHaus,&hnear=Wei%C3%9Fgerberl%C3%A4nde,+Landstra%C3%9Fe+1030+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/쿤스트하우스 <http://qok.me/쿤스트하우스>`_

.. admonition:: |tip-icon| :note-title:`훈데르트밧서`

   .. figure:: /_static/figures/5-25-1.jpg
         :align: center
         :figclass: figure-img

         본명은 프리드리히 슈토바서 Friedrich Stowasser 이지만, 훈데르트밧서 Hundertwasser 로 더 잘 알 려져 있다. 오스트리아 태생의 화가이자 건축가. 비엔나 미술학교를 다니다가 중퇴한 후 여행을 하면서 독자적인 추상화품을 정립했다. 생태주의자로 건축에 관심을 보여 비엔나에 공공주택 훈데르트밧서 하우스를 비롯하여 비엔나에 쓰레기소각장도 설계했다.

.. _5-26:

26 그린찡 선술집
---------------------------------------------------------------------

.. figure:: /_static/figures/5-26.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`그림 26` 그린찡 선술집 거리

호이리게 Heuriger의 역사는 포도재배 농민들이 프란츠 요셉 황제를 상대로 포도주 판매권을 요구하면서 부터 시작된다. 농민들의 요구를 받아들인 프란츠 요셉 황제는 자신이 수확한 포도로 만든 포도주는 직접 판매할 수 있는 권한을 농민들에게 부여했다. 이렇게 되자 포도주를 직 접 판매하는 농가가 하나둘씩 생겨나기 시작했고 포도 재배 단지였던 그린찡은 자연스럽게 호 이리게 마을이 되어 버렸다. ‘호이리게’는 본래 그해 만든 포도주 Heuriger Wein 를 뜻하는 말 이였는데 현재는 그해 만든 포도주를 판매하는 주점의 의미로 사용되고 있다. 그린찡의 호이리 게에는 소속 악사들이 흥을 돋우기 위해 테이블을 돌아다니며 바이올린과 아코디언으로 음악을 연주하기도 한다. 악사들은 한국 사람을 만나면 아리랑이나 애국가 등을 연주하기도 하는데 이 들의 연주들 듣고 나면 팁을 주는 것이 관례이다. 특히, 자신을 위해 연주를 했다면 1곡에 2-3 유로 정도 주는 것이 좋다. 그린찡은 비엔나 외곽에 위치해 있지만 빈 대학교 앞 Schottentor-Universitaet 역에서 38번 전차를 타거나, 아니면 지하철 4호선 종착역인 하일리겐슈타트에서 38A번 버스를 이용해서 갈 수 있다.

:info-item:`주소` `Grinzing <https://maps.google.com/maps?q=Grinzing,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.21089,16.394134&sspn=0.011082,0.015492&oq=grinzing&hnear=Grinzing,+D%C3%B6bling+Klosterneuburg,+Wien-Umgebung,+%EB%8B%88%EB%8D%94%EC%99%B8%EC%8A%A4%ED%84%B0%EB%9D%BC%EC%9D%B4%ED%9E%88+%EC%A3%BC+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=12&iwloc=A>`_

.. _5-27:

27 칼렌베르그
---------------------------------------------------------------------

.. figure:: /_static/figures/5-27.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`그림 27` 칼렌베르그 외부

과거로부터 전략적 요충지였던 칼렌베르그는 빈 숲에 위치한 높은 언덕이면서 484m 높이의 거 대한 산이다. 1683년 오스트리아 폴란드 연합군이 비엔나를 향해 침략해오던 투르크의 군대를 이곳에서 격파했다. 당시 오스트리아 황제 프란츠 요셉 2세는 폴란드의 소비에스키왕에게 감사 를 표하기 위해 칼렌베르그 정상에 위치한 성당의 이름을 소비에스키 교회라고 명명했다. 이곳 에 서면 도나우강과 비엔나 시내가 한 눈에 내려다 보이는데 정상에 있는 카페에서 여유롭게 시 간을 보내는 것도 좋다. 지하철 4호선 종점 하일리겐슈타트에서 38A번 버스를 타면 칼렌베르그에 내릴 수 있다. 가는 길에 그린찡을 지나치게 되므로 반나절 정도의 일정으로 두 곳을 한 번에 둘러볼 수 있다.

:info-item:`주소` `Kahlenberg <https://maps.google.com/maps?q=Kahlenberg,+Vienna,+Austria&hl=ko&sll=48.256111,16.340639&sspn=0.177153,0.247879&oq=kahl&hnear=Kahlenberg&t=m&z=16&iwloc=A>`_

