*********************************************************************
비엔나 링 주변 지역
*********************************************************************

.. include:: ../include.def

비엔나 링 주변을 관람하려면 전차나 지하철과 같은 대중교통을 이용하는 것이 좋다. 일정에 여 유를 갖고 비에나를 돌아볼 작정이라면 입장권 할인도 받고 대중교통을 무료로 이용할 수 있는 비엔나 카드를 구입할 것을 추천한다.

.. _4-11:

11 시민공원
---------------------------------------------------------------------

.. figure:: /_static/figures/4-11.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 11` 시민공원 역

1862년 문을 연 영국식 정원의 시민공원 Stadtpark 은 비엔나 시민과 관광객들의 휴식 공간이 다. 공원 내부에 요한 스트라우스, 슈베르트 등 12명의 기념상이 흩어져 있으며 공원 입구에 요한 스트라우스 연주회가 열렸던 쿠어살롱 Kursalon 이 있다. 아직도 쿠어살롱에서는 요한 슈트 라우스, 모차르트 음악회가 자주 열리고 있는데 인터넷으로 입장권을 예매할 수 있다. 다만, 관 광 성수기인 여름철에는 입장객을 제한하는 고급 나이트 클럽이 문을 열기도 하니 사전에 정보 를 확인하도록 한다. 공원 인근에 암파크링 호텔과 콘체르트하우스, 인터콘티넨탈 호텔을 비롯 한 고급 호텔들이 즐비하다.

오페라 하우스에서 도보로 10분 정도 거리에 있으며 전차를 이용 할 때는 1번과 2번, 지하철은 4호선 Stadtpark 역에서 내리면 된다.

:info-item:`주소` `Landstraßer Hauptstraße 7, 1030 Wien <https://maps.google.com/maps?q=Stadtpark,+Wien,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.207115,16.371217&sspn=0.022966,0.030985&hq=Stadtpark,&hnear=%EB%B9%84%EC%97%94%EB%82%98+%EB%B9%88+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=13&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/시민공원 <http://qok.me/시민공원>`_

.. _4-12:

12 미술사 박물관
---------------------------------------------------------------------

.. figure:: /_static/figures/4-12.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 12` 미술사 박물관 정면

합스부르크 왕가의 수집 담당관리였던 Gottfried Semper 와 Carl Hasenauer 가 미술사 박 물관 Kunsthistorisches Museum 을 1891년 현재의 모습으로 완성했다. 프랑스 루브르 박물관과 더불어 유럽의 3대 미술관중 하나로 평가 받는 미술사 박물관에는 합스부르크 왕가가 수집한 7천여 점에 달하는 방대한 회화 작품과 40만여 점의 각종 미술품이 전시 되어 있다. 라 파엘로, 브뤼겔, 루벤스, 렘브란트 등의 르네상스 거장들의 작품과 고대 그리스, 이집트, 로마의 미술품도 전시 되어 있다. 마리아 테레지아 광장을 사이에 두고 건너편에는 같은 건축가가 지은 동일한 모양의 자연사 박물관이 자리하고 있다.

:info-item:`개장시간` 화~일 10:00~18:00, 목 10:00~21:00, 월요일 휴관

:info-item:`입장료` 7.50유로

:info-item:`주소` `Maria-Theresien-Platz, 1010 Wien <https://maps.google.com/maps?q=Maria-Theresien-Platz,+1010+Wien&hl=ko&sll=48.207046,16.374503&sspn=0.091865,0.12394&hq=Maria-Theresien-Platz,+1010+Wien&t=m&z=14>`_

:info-item:`홈페이지` `http://qok.me/미술사박물관 <http://qok.me/미술사박물관>`_

.. _4-13:

13 자연사 박물관
---------------------------------------------------------------------

.. figure:: /_static/figures/4-13.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 13` 자연사 박물관 정면

자연사 박물관 Naturhistorisches Museum 은 1750년부터 합스부르크 왕가의 수집보관 장 소로 사용되던 곳으로 건축가 Gottfried Semper 와 Carl Hasenauer 의 설계로 1889년 8월 10일 문을 열었다. 이곳의 볼거리는 25,000년 전의 빌렌도르프의 비너스상과 117kg의 거대 한 토파즈 원석, 1,500여개의 다이아몬드로 만든 마리아 테레지아의 보석 부케 등이다.

:info-item:`개장시간` 목~월 09:00~18:30, 수 09:00~21:00, 화요일 휴관

:info-item:`입장료` 10유로

:info-item:`주소` `Burgring 7, 1010 Wien <https://maps.google.com/maps?q=Naturhistorisches+Museum&hl=ko&sll=48.204436,16.360717&sspn=0.045935,0.06197&hq=Naturhistorisches+Museum&t=m&z=16>`_

:info-item:`홈페이지` `http://qok.me/자연사박물관 <http://qok.me/자연사박물관>`_


.. _4-14:

14 국회의사당
---------------------------------------------------------------------

.. figure:: /_static/figures/4-14.jpg
     :align: center
     :figclass: figure-img

     :figure-number:`그림 14` 국회의사당 정면

덴마트 태생의 건축가 테오필 한센 Theophil Hansen 이 그리스 신전 건축을 본 따 설계한 국 회의사당 Parlament 은 1918년 11월 12일 오스트리아 연방 공화국 선언이 이루어진 곳이다. 의사당 앞에는 승리를 상징하는 새를 오른손에 들고 있는 아테네 여신상이 세워져 있는데, 아래 에 있는 4개의 분수는 한때 합스브루크 왕가의 영토에 있던 강들을 상징한다. 비엔나의 도나우 강, 인스브룩의 인강, 함부르크의 엘바강, 프라하의 블타바강. 박물관이나 시청, 빈 대학으로 가 는 길에 있어 쉽게 찾을 수 있다.

:info-item:`주소` `Doktor-Karl-Renner-Ring 3, 1010 Wien <https://maps.google.com/maps?q=Parlament&hl=ko&ll=48.207816,16.360531&spn=0.009681,0.022016&sll=48.205742,16.360531&sspn=0.011483,0.015492&hq=Parlament&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/국회의사당 <http://qok.me/국회의사당>`_

.. _4-15:

15 시청사
---------------------------------------------------------------------

.. figure:: /_static/figures/4-15.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 15` 시청사 정면

시청사 Rathaus 는 1883년 프리드리히 슈미트 Friedrich Schmidt 에 의해 완성된 신 고딕식 건물로 시청사 앞의 광장에서는 일년 내내 크고 작은 행사가 열린다. 독일어로 진행되는 시청사 내부 관광투어는 매주 월, 수, 금 오후 1시에 있으며, 가격은 무료이다.

:info-item:`주소` `Rathausplatz 1, 1010 Wien <https://maps.google.com/maps?q=Rathaus,+Wien,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.206915,16.360531&sspn=0.011483,0.015492&oq=rathaus&hq=Rathaus,+Wien,+%C3%96sterreich&t=m&z=14&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/시청사 <http://qok.me/시청사>`_

.. _4-16:

16 비엔나대학
---------------------------------------------------------------------

.. figure:: /_static/figures/4-16.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 16` 비엔나 대학 외부

비엔나대학 Universität Wien 은 1365년에 루돌프 4와 그 형제들이 주도하여 설립된 독일어 권의 가장 오래된 대학이다. 이태리 르네상스 양식의 현재 대학건물은 1883년 완공된 것으로 촛불성당을 설계한 하인리히 폰 페르스텔l이 건축을 맡았다. 초창기 비엔나대학은 법학, 의학, 철학의 3개의 학과로 운영되다가 60년 후 교황청의 허가를 받아 신학과가 만들어지면서 4개의 학과로 출발하게 되었다. 비엔나대학교의 교수로서 노벨상을 받은 인물은 이제까지 9명이고, 그밖에 비엔나대학교에서 교편을 잡았던 유명인사는 작곡가 안톤 브루크너, 심리학자 지그문트 프로이트, 교황 비오 2세 등이 있다. 또한, 작곡가 겸 지휘자 구스타브 말러, 유전학자 그레고르 멘델, 교황 비오 3세, 물리학자 오토 프레밍거 등은 비엔나대학교 졸업생들이다. 오페라 하우스에서 도보로 약 20분 정도, 지하철은 2번 Schottentor-Universitaet 역에서 하 차한다. 전차는1번이나 2번, D번을 이용하면 된다.

:info-item:`주소` `Doktor-Karl-Lueger-Ring 1, 1010 Wien <https://maps.google.com/maps?q=Universit%C3%A4t+Wien&hl=ko&ie=UTF8&sll=48.213178,16.360102&sspn=0.045927,0.06197&hq=Universit%C3%A4t+Wien&t=m&z=14&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/비엔나대학 <http://qok.me/비엔나대학>`_

.. _4-17:

17 촛불성당
---------------------------------------------------------------------

.. figure:: /_static/figures/4-17.jpg
      :align: center
      :figclass: figure-img

      :figure-number:`그림 17` 촛불성당 외부

1853년 2월 18일 촛불성당 Votivkirche 에서 프란츠 요셉 황제 암살 미수 사건이 발생 하지만 황제의 단추에 흉기가 걸리는 바람에 암살은 미수에 그치고 만다. 훗날 멕시코의 황제가 된 동생 막시밀리안 Erzherzog Ferdinand Maximilian 대공이 황제의 무사함에 감사하며 봉헌했지 만 정작 막시밀리안은 민중혁명 때 처형되어 성당이 완성되는 것을 보지 못했다. 1856년 하인 리히 폰 페르스텔에 의해 시작되어 1879년 신고딕 양식으로 완공 되었다. 촛불성당 앞의 광장 에서는 월드컵이나 올림픽 같은 큰 경기가 있을 때 대형 스크린을 설치해서 많은 사람들이 함께 응원하며 관람한다.

:info-item:`주소` `Rooseveltplatz, 1090 Vienna <https://maps.google.com/maps?q=votivkirche&hl=ko&ll=48.213349,16.360102&spn=0.03872,0.088062&sll=48.213175,16.36007&sspn=0.045927,0.06197&hq=votivkirche&t=m&z=14&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/촛불성당 <http://qok.me/촛불성당>`_

.. admonition:: |tip-icon| :note-title:`막시밀리안 황제`

   .. figure:: /_static/figures/4-17-1.jpg
         :align: center
         :figclass: figure-img

         막시밀리안 황제 Maximiliano I (1832-1867)는 멕시코 제국의 황제이자 오스트리아 대공으로 프랑스 황 제 나폴레옹 3세의 조카이기도 하다. 오스트리아 황제 프란츠 요셉 1세의 동생으로 오스트리아 해군소장으 로 복무하다가 당시 프랑스령이였던 멕시코 제국의 초청으로 황제가 되었다. 그러나 멕시코 재정 파탄과 내 분, 그리고 미국의 먼로주의로 인해 취임 3년 만에 반군에게 체포되어 총살되고 만다.
