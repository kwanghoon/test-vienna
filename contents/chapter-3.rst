*********************************************************************
비엔나 링 내부 지역
*********************************************************************

.. include:: ../include.def


비엔나는 우리에게 음악의 도시로 흔히 알려져 있지만, 잘 살펴보면 정신분석학의 창시자 지그 문트 프로이드, 건축가 아돌프 로스와 오토 바그너, 그리고 화가 구스타브 클림트도 비엔나가 배출한 대표적인 천재들이다. 과거 비엔나를 통치했던 합스부르크 왕가와 천재 예술가들의 흔 적을 보려면 비엔나 링 Vienna Ring 을 중심으로 관광지를 나누어서 살펴보는 것이 이 바람직 하다.

.. _3-1:

1 오페라하우스
---------------------------------------------------------------------

.. figure:: /_static/figures/3-1.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 1` 오페라하우스 건물

파리 오페라 극장, 밀라노 스칼라 극장과 함께 유럽의 3대 오페라 극장으로 인정받고 있는 비엔 나의 오페라 하우스 Staatsopera 는 1869년 5월 15일 모짜르트의 돈조바니 공연을 시작으로 개관했다. 이후, 구스타브 말러를 비롯해 우리나라 애국가를 작곡한 안익태의 스승인 리하드 슈 트라우스, 바움가르트너 등 세계적인 음악가들이 오페라 하우스의 총감독으로 거쳐 갔다. 음악 의 도시인 비엔나 답게 평소에 많은 연주회가 있지만 짤스부르크 음악축제가 열리는 7, 8월에 는 비엔나 필의 연주를 들을 수가 없다. 만약 오페라 공연을 보고 싶다면 2~3.5유로를 내고 입 석표를 사면 된다. 그러나 공연 두세 시간 전부터 표를 사는 줄이 길게 늘어서므로 서둘러야 구 입할 수 있다. 공연은 보지 않고 극장 내부 만을 둘러볼 계획이라면 낮 시간에 운영하는 유료 가 이드투어를 이용하면 된다.

오페라 하우스는 지하철 1번, 2번, 4번 Karlsplatz 역에서 내려 OPER 방향으로 계단을 올라가 면 오페라 하우스와 만난다. 오페라 하우스부터 슈테판 성당 까지를 케른트너 거리라고 하는데 보행자 전용 거리로 비엔나 최고의 중심가이다.

:info-item:`주소` `Opernring 2, 1010 Wien <https://maps.google.com/maps?q=Opernring+2,+1010+Wien&hl=en&sll=48.210261,16.361217&sspn=0.018932,0.045447&hnear=Opernring+2,+Innere+Stadt+1010+Wien,+Austria&t=m&z=17>`_

:info-item:`홈페이지` `http://qok.me/오페라하우스 <http://qok.me/오페라하우스>`_

.. admonition:: |tip-icon| :note-title:`케른트너 거리`

   케른트너 거리는 비엔나 최고의 중심 거리지만 보행자 전용 거리이다. 길을 걷다보면 거리 악사들이 연주를 하거나 작은 공연들이 수시로 펼쳐지는 것을 볼 수 있다. HABEN을 비롯한 최고급 보석가게와 카지노가 들어서 있고 비발디가 숨을 거둔 아파트와 슈테플 백화점도 이 거리에 있다.

.. _3-2:

2 슈테판 성당
---------------------------------------------------------------------

.. figure:: /_static/figures/3-2.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 2` 슈테판 성당 외부

1147년 로마네스크 양식으로 시작하여 고딕 양식으로 완성된 슈테판 성당 St. Stephansdom 은 모짜르트의 결혼식과 장례식이 치루어진 곳으로 유명하다. 기독교 최초의 순교자 이름을 딴 슈테판 성당은 137m의 첨탑과 25만개의 벽돌(기와)로 만들어진 모자이크 지붕이 특징이다. 성당 지하에 있는 유골실에는 오스트리아 황제들의 내장을 넣어둔 항아리와 백골이 보관되어 있다.

지하철 1번이나 3번을 타고 Stephansplatz 역에서 내리면 된다. 매년 실베스터(12월 31일)에 는 사람들이 슈테판 광장에 모여 새해를 맞는 풍습이 있는데 이때 와인을 마시고 와인 잔은 바닥에 깨트리는 전통이 있다.

:info-item:`주소` `Stephansplatz 1, 1010 Wien <https://maps.google.com/maps?q=Stephansdom,+Wien,+%C3%96sterreich&hl=en&ie=UTF8&sll=48.204763,16.369178&sspn=0.004734,0.011362&oq=stepha+Wien&hq=Stephansdom,+Wien,+%C3%96sterreich&t=m&z=15&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/슈테판성당 <http://qok.me/슈테판성당>`_

.. _3-3:

3 하스 하우스
---------------------------------------------------------------------

.. figure:: /_static/figures/3-3.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 3` 하스 하우스 입구

오스트리아 건축가 한스 홀라인의 하스 하우스 Haas-Haus 는 1990년 완공 당시부터 중세 건 축물이 즐비한 거리 분위기와 비교해 지나치게 현대적이라는 논란을 불러일으켰다. 콘크리트 프레임에 대리석과 유리가 조화를 이룬 하스 하우스는 현재 상점들과 레스토랑이 입주해 있다.

:info-item:`주소` `Stephansplatz 4, 1010 Vienna <https://maps.google.com/maps?q=Haas-Haus,+Stephansplatz,+Wien,+%C3%96sterreich&hl=en&ie=UTF8&sll=48.208258,16.373277&sspn=0.018933,0.045447&oq=haas+,+Wien,+%C3%96sterreich&hq=Haas-Haus,+Stephansplatz,+Wien,+%C3%96sterreich&radius=15000&t=m&z=14>`_

:info-item:`홈페이지` `http://qok.me/하스하우스 <http://qok.me/하스하우스>`_

.. _3-4:

4 성 피터 교회
---------------------------------------------------------------------

.. figure:: /_static/figures/3-4.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 4` 성 피터 교회 정면

슈테판 성당에 비해 규모가 작은 성 피터 교회 St. Peterkirche 는 모차르트가 미사곡을 연주했 던 곳으로 알려져 있다. 전쟁과 화재로 몇 번 소실되었다가 레오폴드 1세가 비엔나에 페스트가 창궐했을 때 재건축을 선언하면서 현재의 건물이 완성되었다. 케른트너 거리와 콜마크트를 이 어주는 그라벤 거리에 있다.

:info-item:`주소` `Petersplatz, 1010 Wien <https://maps.google.com/maps?q=Petersplatz,+1010+Wien&hl=en&hnear=Petersplatz,+Innere+Stadt+1010+Wien,+Austria&t=m&z=17>`_

:info-item:`홈페이지` `http://qok.me/성피터교회 <http://qok.me/성피터교회>`_

.. admonition:: |tip-icon| :note-title:`그라벤`

   케른트너 거리와 콜마크트를 잇는 그라벤 거리의 트레이드 마크는 레오폴드 1세가 세운 페스트 종식 기념 탑이다. 당시 유럽을 공포로 몰아넣은 페스트로 비엔나에서만 15만명의 희생자가 발생했다는 기록이 있다. 그라벤 거리는 비엔나에서 처음으로 가스 가로등이 설치되기도 했으며 에어스테 은행 앞 공중 화장실에 는 세계에서 가장 오래된 변기가 지금도 사용되고 있다. 그라벤 10번지에 있는 앙커 하우스는 1894년 완성 된 당대의 최고 건축가 오토 바그너의 작품이다. 슈테판 성당에서 그라벤이 끝나갈 무렵 왼쪽으로 돌면 비 엔나의 명품거리 콜마크트와 오른쪽으로 가면 비엔나에서 가장 큰 암 호프 광장과 만나게 된다.

.. _3-5:

5 카푸치너 교회
---------------------------------------------------------------------

.. figure:: /_static/figures/3-5.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 5` 카푸치너 교회 정면

카푸치너 교회 Kapuzinerkirche 의 황제 납골당 Kaisergruft은 오스트리아 합스부르그 왕가 의 가족묘로 조성되어 있다. 마리아 테레지아 여제, 프란츠 요셉 황제, 엘리자베스 황비 등 오스 트리아의 역사적인 인물들이 안장되어 있다. 황제 납골당에 안장된 무덤 중 마리아 테레지아 여 제의 관이 가장 큰데 그 이유는 그녀가 생전에 남편 로트링엔과의 합장을 유언으로 남겼기 때문 이다. 황제 일가만이 묻힐 수 있는 이곳에서 황제나 왕족이 아닌 사람은 마리아 테레지아 여제 의 보모가 유일하다. 납골당에 입장하려면 2.8 ~ 3.9유로를 내야 한다.

:info-item:`주소` `Neuer Markt, 1010 Wien <https://maps.google.com/maps?q=Capuchin+Church,+Vienna&hl=ko&ie=UTF8&sll=48.208271,16.36635&sspn=0.011483,0.014012&hq=Capuchin+Church,&hnear=%EB%B9%84%EC%97%94%EB%82%98+%EB%B9%88+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

.. _3-6:

6 로스 하우스
---------------------------------------------------------------------

.. figure:: /_static/figures/3-6.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 6` 로스 하우스 외부

“장식은 범죄다”라고 주장했던 아돌프 로스가 설계한 주상복합건물이다. 미카엘 광장을 사이 에 두고 왕궁과 마주보는 로스 하우스 Loos Haus 는 1911년 완공되기 전부터 비엔나시와 건물 장식을 두고 신경전을 벌인 것으로 유명하다. 당시 비엔나 언론도 비엔나 왕궁 앞에 아무 장 식도 없는 건축물이 들어서는 것에 대해 비판적이었을 정도로 로스 하우스는 비엔나 건축 전통 에 대한 일종의 반역으로 여겨졌다. 심지어 프란츠 요젭 황제는 격분하여 로스 하우스 앞에 있 는 미카엘 문으로 왕궁을 출입하지 않을 것을 선언하기 까지 했다. 그러나 오늘날 로스 하우스 는 비엔나 현대 건축물을 상징하며 그 자리를 꿋꿋이 지키고 있다.

:info-item:`주소` `Herrengasse 2, 1010 Wien <https://maps.google.com/maps?q=Herrengasse+2,+1010+Vienna,+Austria&hl=ko&ie=UTF8&sll=48.20271,16.366582&sspn=0.011484,0.014012&hnear=Herrengasse+2,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=16&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/로스하우스 <http://qok.me/로스하우스>`_

.. admonition:: |tip-icon| :note-title:`콜마크트`

   14세기 목탄을 판매하던 거리였던 콜마크트는 현재 구찌, 샤넬, 까르티에, 루이비통 등 세계적인 유명 브랜 드샵들이 포진해 있는 비엔나의 명품거리로 변모했다. 콜마크트 11번지에는 하이든이 젊은 시절 살았던 미 카엘하우스 Michaelerhaus 와 빈에서 케이크의 종류가 가장 많다는 데멜 Demel 빵집이 있다. 콜마크트 를 지나 미카엘 광장을 가로지르게 되면 구왕궁 Hofburg 로 들어서게 된다.

.. _3-7:

7 왕궁
---------------------------------------------------------------------

.. figure:: /_static/figures/3-7.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 7` 구왕궁 Hofburg

합스부르크 왕가의 궁전으로 구 왕궁 Hofburg, 신 왕궁 Neue Burg, 황실 아파트먼트, 왕실 보물관, 왕궁 예배당, 스페인 승마학교, 아우구스티너 교회 등의 부속건물과 그에 딸린 정원으로 구성되어 있다. 왕궁 내의 건축물들은 여러 세기에 걸쳐 오랜 동안 지어졌기 때문에 건축양 식이 제각각이다.

:link-title:`1)왕궁 예배당`

왕궁 예배당 Burgkapelle 은 13세기에 바로크 양식으로 건설된 건축물이지만 15세기에 와서 고딕양식으로 보수되었다. 9월부터 6월까지 기간 동안 매주 일요일 오전 9시 15분에 열리는 미사에서 비엔나 소년합창단의 공연을 볼 수 있다. 좌석표의 가격은 5~29유로 이며, 사전에 이 메일 office@hofburgkapelle.at 이나 온라인 으로 예약할 수 있다. 입석표는 무료이지만 당 일 오전 8시 30분부터 줄을 서야 구할 수 있다.

:info-item:`주소` `Michaelerkuppel, 1010 Wien <https://maps.google.com/maps?q=burgkapelle,+vienna&hl=ko&ie=UTF8&ll=48.208516,16.365037&spn=0.019362,0.044031&sll=48.207115,16.365037&sspn=0.022966,0.030985&hq=burgkapelle,&hnear=%EB%B9%84%EC%97%94%EB%82%98+%EB%B9%88+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=15&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/왕궁예배당 <http://qok.me/왕궁예배당>`_

:info-item:`온라인 예매` `http://qok.me/왕궁예배당온라인예매 <http://qok.me/왕궁예배당온라인예매>`_

:link-title:`2) 황실 아파트먼트`

프란츠 요셉 황제와 씨씨 황후 일가가 살았던 황실 아파트먼트 Kaiserappartements 는 황제 집무실, 황후 침실, 각료 회의실 등을 관람객들에게 황후 씨씨의 유품을 소장한 씨씨 박물관과 은제 박물관도 내부에 있어 한번에 둘러볼 수 있다.

:info-item:`주소` `Michaelerkuppel, 1010 Wien <https://maps.google.com/maps?q=burgkapelle,+vienna&hl=ko&ie=UTF8&ll=48.208516,16.365037&spn=0.019362,0.044031&sll=48.207115,16.365037&sspn=0.022966,0.030985&hq=burgkapelle,&hnear=%EB%B9%84%EC%97%94%EB%82%98+%EB%B9%88+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=15&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/황실아파트먼트 <http://qok.me/황실아파트먼트>`_

:link-title:`3) 스페인 승마학교`

세계에서 가장 오래된 승마학교로 1572년 합스부르크 왕가가 스페인에서 들여온 말을 훈련시 키면서 기수들을 키워내기 위해 설립했다. 이 학교는 신입생을 5년에 한 번씩 딱 4명만 선발할 정도로 입학이 어려운데, 2008년 최초로 여학생이 입학하면서 다시 한번 유명세를 치뤘다. 관 광객을 상대로 한 공연과 이벤트는 홈페이지에 들어가서 온라인으로 예약할 수 있다.

:info-item:`주소` `Michaelerplatz 1, Hofburg, 1010 Vienna <https://maps.google.com/maps?q=Spanish+Riding+School,+Vienna,+Austria&hl=ko&sll=48.208273,16.36699&sspn=0.012784,0.0156&oq=spanish++Vienna,+Austria&hq=Spanish+Riding+School,+Vienna,+Austria&radius=15000&t=m&z=13&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/스페인승마학교 <http://qok.me/스페인승마학교>`_

:link-title:`4) 아우구스티너 교회`

아우구스티너 교회 Augustinerkirche 는 14세기 고딕양식의 건물로 왕가의 결혼식과 장례식 장소로 사용되었다. 1810년 마리 루이지와 나폴레옹, 1851년 프란츠 요셉 1세와 씨씨 황후도 이곳에서 결혼식을 올렸다. 지하에는 오스트리아 황제들의 심장을 보존하고 있는 납골당이 있 다. 참고로 합스부르크 왕가는 황제의 심장은 아우구스티너 교회, 내장은 슈테판 성당, 나머지 는 카푸치너 교회에 나누어서 안치했다.

:info-item:`주소` `Innere Stadt, 1010 Vienna <https://maps.google.com/maps?q=Augustinerkirche&hl=ko&sll=48.20743,16.365037&sspn=0.022966,0.030985&hq=Augustinerkirche&t=m&z=14&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/아우구스티너교회 <http://qok.me/아우구스티너교회>`_

.. _3-8:

8 궁정 극장
---------------------------------------------------------------------

.. figure:: /_static/figures/3-8.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 8` 궁정 극장 정면

본래 구 왕궁 옆에 있던 연회장 건물이였던 궁정극장 Burgtheater 은 마리아 테레지아 여제가 1741년 극장으로 사용하는 것을 허락하면서 그 역사가 시작되었다. 마리아 테레지아 여제의 아들 요셉 2세 황제가 즉위하면서 비엔나 최초의 정식 극장이 되었고 독일어 연극을 상연하기 시 작했다. 극장의 천장에는 클림트가 그리스 비극을 묘사하여 그린 프레스코화가 있다.

:info-item:`주소` `Doktor-Karl-Lueger-Ring 2, 1010 Wien <https://maps.google.com/maps?q=burgtheater&hl=ko&ll=48.209231,16.365037&spn=0.019362,0.044031&sll=48.207115,16.365037&sspn=0.022966,0.030985&hq=burgtheater&t=m&z=15&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/궁정극장 <http://qok.me/궁정극장>`_

.. _3-9:

9 앙커시계
---------------------------------------------------------------------

.. figure:: /_static/figures/3-9.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 9` 앙커 시계 정면

호어 마크트 Hoher Markt 거리에 있는 앙커시계 Ankeruhr 는 앙커 보험회사의 두 건물을 이 어주는 다리 부분을 장식하기 위해 제작되었다. 아르누보 양식의 독특한 시계는 매일 12시 정오가 되면 마리아 테레지아 여제 등 12명의 역사적인 오스트리아 인물들이 차례로 등장하면서 시 간을 알려주기 때문에 이 광경을 구경하려는 관광객들의 발걸음이 연중 끊이지 않는 곳이다.

:info-item:`주소` `Hoher Markt 4, 1010 Wien <https://maps.google.com/maps?q=Ankeruhr&hl=ko&ll=48.210318,16.367569&spn=0.038723,0.088062&sll=48.205913,16.367556&sspn=0.045933,0.06197&hq=Ankeruhr&t=m&z=14&iwloc=A>`_

:info-item:`홈페이지` `http://qok.me/앙커시계 <http://qok.me/앙커시계>`_

.. admonition:: |tip-icon| :note-title:`호어 마크트 광장`

   비엔나에서 가장 오래된 호어 마크트 Hoher Markt 광장은 로마시대 유적이 남아 있는데 13세기에는 어시 장으로, 합스부르그 왕가 시대에는 공개 처형장으로 사용되기도 했다. 호어 마크트 광장에 있는 분수는 레오폴드 1세가 아들 요셉 왕자가 전쟁포로로 잡혀 있다가 무사히 귀환하는 것을 기념하기 위해 세웠다. 피 셔 폰 에어라흐 Fischer von Erlach 가 설계하고 그의 아들이 1732년에 완성했다.


.. _3-10:

10 암 호프 광장
---------------------------------------------------------------------

프라하 유대인 지구 Josefov 는 13세기부터 프라하에 터를 잡은 유대인들이 모여 살던 곳으로 현재는 유대인 교회당, 집회소, 공동묘지 등이 남아 그 흔적을 보여준다. 프라하에 있는 유대인 지구는 유럽에서 가장 보존이 잘 된 편이기 때문에 유대민족의 역사를 보여주는 박물관과 교육 센터로 활용되고 있다.

.. figure:: /_static/figures/3-10.jpg
   :align: center
   :figclass: figure-img

   :figure-number:`그림 10` 암 호프 광장

1156년 바벤베르그 왕조의 성이 있던 자리로 현재 비엔나에서 가장 큰 광장이다. 암 호프는 Am Hof 광장은 합스부르크 왕가의 프란츠 2세가 신성 로마제국 황제에서 물러나는 것을 선언 한 뼈아픈 역사의 현장이기도 하다. 1667년에는 마리아상이 세워졌으며 현재는 광장을 중심으 로 암호프 성당과 은행이 들어서 있다.

:info-item:`주소` `Am Hof 1010 Wien <https://maps.google.com/maps?q=Am+Hof,+Wien,+%C3%96sterreich&hl=ko&ie=UTF8&sll=48.206715,16.367569&sspn=0.045933,0.06197&oq=Am+hof&hnear=Am+Hof,+Innere+Stadt+1010+Wien,+%EC%98%A4%EC%8A%A4%ED%8A%B8%EB%A6%AC%EC%95%84&t=m&z=15&iwloc=A>`_
