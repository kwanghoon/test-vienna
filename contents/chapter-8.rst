
*********************************************************************
지도로 보는 비엔나
*********************************************************************

.. include:: ../include.def

- 장소 앞에 표기된 번호와 색을 먼저 확인하고 지도에서 해당 아이콘을 찾으세요.

- 넷스코미디어랩 자료실에 오시면 높은 해상도의 지도를 다운 받으실 수 있습니다.

.. figure:: /_static/figures/8-1.png
      :align: center
      :figclass: full-img

:link-title:`1. 비엔나 링 내부 지역`

:ref:`3-1`

:ref:`3-2`

:ref:`3-3`

:ref:`3-4`

:ref:`3-5`

:ref:`3-6`

:ref:`3-7`

:ref:`3-8`

:ref:`3-9`

:ref:`3-10`

:link-title:`2. 비엔나 링 주변 지역`

:ref:`4-11`

:ref:`4-12`

:ref:`4-13`

:ref:`4-14`

:ref:`4-15`

:ref:`4-16`

:ref:`4-17`

:link-title:`3. 비엔나 링 외부 지역`

:ref:`5-18`

:ref:`5-19`

:ref:`5-20`

:ref:`5-21`

:ref:`5-22`

:ref:`5-23`

:ref:`5-24`

:ref:`5-25`

:ref:`5-26`

:ref:`5-27`

:link-title:`숙소`

:ref:`6-h-1`

:ref:`6-h-2`

:ref:`6-h-3`

:ref:`6-h-4`

:ref:`6-h-5`

:ref:`6-h-6`

:ref:`6-h-7`

:link-title:`맛집`

:ref:`6-c-1`

:ref:`6-c-2`

:ref:`6-c-3`

:ref:`6-c-4`

:ref:`6-c-5`

:ref:`6-c-6`

:ref:`6-c-7`

:link-title:`쇼핑`

:ref:`6-s-1`

:ref:`6-s-2`

:ref:`6-s-3`

:ref:`6-s-4`

:ref:`6-s-5`

.. figure:: /_static/figures/8-0.jpg
      :align: center
      :figclass: full-img