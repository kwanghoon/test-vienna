# -*- coding: utf-8 -*-

# NETSCO MEDIA BOOK

import sys, os

sys.path.insert(0, os.path.abspath('.'))

from book import *

extensions = []
templates_path = ['_templates']
source_suffix = '.rst'
source_encoding = 'utf-8-sig'
master_doc = 'index'

language = 'ko'
exclude_patterns = ['_build']
pygments_style = 'manni'


# -- Options for HTML output ---------------------------------------------------

# html_theme = 'default'
html_theme = 'netsco-html'
html_theme_path = ['./themes/']

#html_theme_options = {}
#html_title = None

#html_favicon = None
html_static_path = ['_static']
html_last_updated_fmt = '%b %d, %Y'
#html_use_smartypants = True
#html_sidebars = {}
#html_additional_pages = {}
#html_domain_indices = True
html_use_index = True
#html_split_index = False
#html_show_sourcelink = True
html_show_sphinx = False
html_show_copyright = True
#html_use_opensearch = ''
#html_file_suffix = None
htmlhelp_basename = 'book'


# -- Options for Epub output ---------------------------------------------------

epub_language = 'ko'
epub_theme = 'netsco-epub'
#epub_scheme = ''
#epub_uid = ''
#epub_pre_files = []
#epub_post_files = []

#epub_exclude_files = []
#epub_tocdepth = 3
#epub_tocdup = True