#!/bin/bash
#
# Watch app folders
#
compass watch -c "themes/netsco-html/static/config.rb" --force &
compass watch -c "themes/netsco-epub/static/config.rb" --force &