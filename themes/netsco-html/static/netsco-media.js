//
// netsco-media.js
// ~~~~~~~~~~~
//
// Sphinx javascript (netsco-media)
//
// :copyright: Copyright 2012 by Netsco, Inc.
//
$(document).ready(function(){
    if (navigator.userAgent.indexOf('iPhone') > 0 ||
        navigator.userAgent.indexOf('Android') > 0) {
        $("div.related ul li:not(.right) a").text("Top");
    }
});

$(window).resize(function(){
    if ($(window).width() <= 480) {
        $("div.related ul li:not(.right) a").text("Top");
    }
    else {
        $("div.related ul li:not(.right) a").text("{{ shorttitle|e }}");
    }
});