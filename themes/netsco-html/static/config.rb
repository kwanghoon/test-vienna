# -*- coding: utf-8 -*-
#
# Netsco Medial Lab
# Compass coniguration file
#
# Copyright 2012 Netsco, Inc.

css_dir        = File.dirname(__FILE__)
sass_dir       = File.dirname(__FILE__)
images_dir     = File.dirname(__FILE__)
javascript_dir = File.dirname(__FILE__)

# output_style = :expanded or :nested or :compact or :compressed
# relative_assets = true
