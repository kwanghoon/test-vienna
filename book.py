# -*- coding:utf-8 -*-*
#
# BOOK CONFIGURATION
#

project   = u'비엔나' # EPUB filename 'book.epub'
copyright = u'넷스코 미디어랩'
version   = '1.0' # CONTENTS VERSION
release   = version # RELEASE VERSION (EXTERNAL)

#--------------------------------------------------------------------
# HTML
#--------------------------------------------------------------------
html_short_title = u"비엔나" # HTML NAVIGATION TITLE
html_logo = '_static/logo.jpg' # HTML LOGO IMAGE
#--------------------------------------------------------------------

#--------------------------------------------------------------------
# EPUB - Bibliographic Dublin Core info.
#--------------------------------------------------------------------
epub_title     = u'비엔나'
epub_author    = u'Netsco Media Lab'
epub_publisher = u'Netsco Media Lab'
epub_copyright = u'Netsco Media Lab'

epub_identifier = 'ISBN' # ISBN or Web URL
epub_scheme = 'ISBN' # (choices) ISBN, URL
#--------------------------------------------------------------------

#--------------------------------------------------------------------
# COVER PAGE
#--------------------------------------------------------------------
epub_cover = ('_static/logo.jpg', 'epub-cover.html')
#--------------------------------------------------------------------
